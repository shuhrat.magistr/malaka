<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubOrganization extends Model
{
    protected $table = 'sub_organizations';
}
