<?php

namespace App\Http\Controllers;

use App\Model\Organization;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    public function index()
    {
        $organizations = Organization::all();
        return view('organization', compact('organizations'));
    }
}
